//Syntax, Statement, and Comments
//For us to create a comment, we use Ctrl + /

console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

//Statements - in programming are instruction tha we tell the computer to perform
//JS Statements usually end with semicolon (;)

//Syntax - it is the set of rules that describes how statements must be constructed

//Comments
//For us to create a comment, we use Ctrl +/
//We have single line and multi-line comments (ctrl + /) multiple comments (ctrl + shift + /)

//console.log("Hello!");

/*alert("This is an alert!");
alert("This is another alert");*/

//Whitespaces (spaces and line breaks) can impact functionality in many computer languages, but not in JS


//Variables
//This is used to contain data

//Declare variables
	//Syntax
		//let/const variableName;

let myVariable;
	console.log(myVariable)

	//Variables must be declared first before they are used
	//

	let hello;
	console.log(hello);

	let  productName = 'desktop computer';
	console.log(productName);

	let productPrice = 18999;
	console.log(productPrice);

	const interest = 3.539;

	//re-assigning variavble values

	productName = 'Laptop';
	console.log(productName);

	//interest = 3.5;
	//console.log(interest);

	//let variable cannot be re-declared witihn its scope
	let friend = 'kate';
	friend = 'Jane';

	//Declare a variable
	let supplier;

	//Initialization
	supplier = "John Smith Tradings";

	//This is considered Re-assignnment because its initial value was already declared
	supplier = "Zuitt Store"

	//We cannot declare a const variable without initialization.
	/*const pi;
	pi = 3.1416;
	console.log(pi);*/

	//Multiple variable declarations

	let productCode = 'DC017', productBrand = 'Dell';

	//let productCode = 'DC017';
	//const productBrand = 'Dell';
	console.log(productCode, productBrand);

	//Data Types

	//Strings
	//Strings are a series of characters that create a word, a phrase or a sentence or anything related to creating tex

	let country = 'Philippines';
	let province = 'Metro Manila';

	//Concatenating Strings

	let fullAddress = province + ', ' + country;
	console.log(fullAddress);

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	//the escape character (\) in strings in combination with other characters can produce different effects

	//"\n" refers to creating a new line in between text

	let mailAddress = 'Metro Manila\n\n\nPhilippines';
	console.log(mailAddress);

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employees wne home early';
	console.log(message);

	//Numbers

	let headcount = 26;
	console.log(headcount);
	let grades = 98.7;
	console.log(grades);
	let planetDistance = 2e10;
	console.log(planetDistance)

	//Combine text and Strings
	console.log("John's first grade last quarter is " + grades);

	//Boolean
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);

	console.log("isGoodConduct: " + isGoodConduct);

	//Arrays
	let grade = [98.7,92.1,90.7,98.6];
	console.log(grades);

	//Objects
		//syntax
		//let/const
		//propertyA

	let myGrades ={
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.7,
		fourthGrading: 94.6,

	}

	console.log(myGrades)

	let person = {

		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ["+639123456789","87000"],
		address: {
			houseNumber:'345',
			city:'Manila'
		}
	}
	console.log(person);

	//type of operator

	console.log(typeof person);

//null
	// it is used to intentionally express the absence of the value in variable declaration/initialization
	
	let spouse = null;

//undefined
	// this represents the state of a variable that been declared but without an assigned value

	let fullName;
	console.log(fullName);//undefined

	let dog = "Happy";
	console.log(dog);

	let message = "Hi, My name is Jc";
	console.log (message);
	